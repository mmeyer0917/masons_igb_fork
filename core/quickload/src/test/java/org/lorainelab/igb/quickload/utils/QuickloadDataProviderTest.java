package org.lorainelab.igb.quickload.utils;

import com.affymetrix.genometry.GenomeVersion;
import com.google.common.base.Strings;
import org.lorainelab.igb.quickload.QuickloadDataProvider;
import org.lorainelab.igb.synonymlookup.services.SpeciesInfo;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author dcnorris
 */
public class QuickloadDataProviderTest {

    private static QuickloadDataProvider dataProvider;
    private static GenomeVersion version;

    @BeforeClass
    public static void setup() throws InterruptedException {
        GenomeVersion genomeVersion = new GenomeVersion("Quickload sample");
        dataProvider = new QuickloadDataProvider("http://igbquickload.org/", "igbquickload", 1);
        Thread.sleep(1000);
        dataProvider.setMirrorUrl("http://bioviz.org/quickload/");
        version = new GenomeVersion("A_thaliana_Jun_2009");
        dataProvider.initialize();
    }

    @Test
    public void validateSpeciesInfo() {
        final Optional<Set<SpeciesInfo>> speciesInfo = dataProvider.getSpeciesInfo();
        assertTrue(speciesInfo.isPresent());
        speciesInfo.ifPresent(infoSet -> {
            infoSet.stream().forEach(info -> {
                assertFalse(Strings.isNullOrEmpty(info.getName()));
                assertFalse(Strings.isNullOrEmpty(info.getCommonName()));
                assertFalse(Strings.isNullOrEmpty(info.getGenomeVersionNamePrefix()));
            });
        });
    }

    @Test
    public void validateSupportedGenomeVersionNames() {
        final Set<String> supportedGenomeVersionNames = dataProvider.getSupportedGenomeVersionNames();
        assertTrue(supportedGenomeVersionNames.contains("A_thaliana_Jun_2009"));
        assertTrue(supportedGenomeVersionNames.contains("H_sapiens_Dec_2013"));
    }

    @Test
    public void testAssemblyInfo() {
        Map<String, Integer> assemblyInfo = dataProvider.getAssemblyInfo(version);
        //JDaly - Changed from "chr5" in IGBF-1142 to match output from AssemblyInfo
        assertTrue(assemblyInfo.containsKey("Chr5"));
        assertTrue(assemblyInfo.containsKey("ChrM"));
    }

    @Test
    public void checkMirrorUrl() {
        assertTrue(dataProvider.getMirrorUrl().isPresent());
        assertTrue(dataProvider.getMirrorUrl().get().equals("http://bioviz.org/quickload/"));
    }

}
